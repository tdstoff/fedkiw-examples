clear all;
%
% Parameters
champspath   = '.';
yloc         = 0.01;
zloc         = 0.0;
nvar         = 4;
pinf         = 101325;
Tinf         = 300;
casestorun   = [1];
updateuser   = 1;
updateamr    = 1;
updatepoints = 1;
dorun        = 1;
updatedata   = 1;
doreadlsf    = 0;
updateslides = 1;
np           = [1      ,1      ,1      ,1      ,1      ,1      ,1      ,1      ,6      ,1      ,6      ];
example      = [1      ,1      ,1      ,2      ,2      ,2      ,2      ,2      ,2      ,2      ,5      ];
test         = ["N/A"  ,"N/A"  ,"N/A"  ,"A"    ,"A"    ,"A"    ,"B"    ,"B"    ,"B"    ,"D"    ,"N/A"  ];
xmin         = [0.0    ,0.0    ,0.0    ,0.0    ,0.0    ,0.0    ,0.0    ,0.0    ,0.0    ,0.0    ,0.0    ];
xmax         = [4.0    ,4.0    ,4.0    ,1.0    ,1.0    ,1.0    ,1.0    ,1.0    ,1.0    ,1.0    ,10.0   ];
nprobes      = [50     ,50     ,50     ,50     ,50     ,50     ,50     ,50     ,50     ,50     ,50     ];
R            = [287.15 ,287.15 ,287.15 ,287.15 ,287.15 ,287.15 ,287.15 ,287.15 ,287.15 ,287.15 ,332.825;
                287.15 ,287.15 ,287.15 ,287.15 ,287.15 ,287.15 ,2077.0 ,2077.0 ,2077.0 ,2077.0 ,2077.0 ];
gamma        = [1.4    ,1.4    ,1.4    ,1.4    ,1.4    ,1.4    ,1.4    ,1.4    ,1.4    ,1.4    ,1.25   ;
                1.4    ,1.4    ,1.4    ,1.2    ,1.2    ,1.2    ,1.667  ,1.667  ,1.667  ,1.667  ,7.15   ];
cp           = [1005.0 ,1005.0 ,1005.0 ,1005.0 ,1005.0 ,1005.0 ,1005.0 ,1005.0 ,1005.0 ,1005.0 ,1517.6 ;
                1005.0 ,1005.0 ,1005.0 ,1005.0 ,1005.0 ,1005.0 ,5193.0 ,5193.0 ,5193.0 ,5193.0 ,4220.0 ];
isTait       = [0      ,0      ,0      ,0      ,0      ,0      ,0      ,0      ,0      ,0      ,0      ;
                0      ,0      ,0      ,0      ,0      ,0      ,0      ,0      ,0      ,0      ,1      ];
ATait        = 1.0e5;
BTait        = 3.31e8;
rho0Tait     = 1000.0;
xint         = [4.0    ,2.0    ,2.0    ,0.5    ,0.5    ,0.5    ,0.5    ,0.5    ,0.5    ,0.5    ,5.0    ];
nx           = [100    ,100    ,100    ,100    ,100    ,100    ,100    ,100    ,400    ,100    ,500    ];
nperblock    = 10;
ny           = [10     ,10     ,10     ,10     ,10     ,10     ,10     ,10     ,10     ,10     ,10     ];
%
% Pre-processing
disp(' ');
disp(['   !----- Script running Fedkiw example case(s)  -----!']);
disp(' ');
disp(['Cases to run: [',num2str(casestorun),']']);
disp(' ');
cv = cp - R;
%
% Optionally clear previous outputs
prompt = 'Do you want to clear the CHAMPS output folder before continuing? Y/N [Y]: ';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end
if (str == 'Y')
    doclearoutput = 1;
else
    doclearoutput = 0;
end
if (doclearoutput)
    system(['rm ',champspath,'/debug/* ',champspath,'/post/*']);
end
%
% Cases loop
for j = casestorun
    disp(' ');
    disp(['          !--- Case ',num2str(j),' = Example ',num2str(example(j)),' Test ',num2str(test(j)),' ---!']);
    disp(' ');
    [datain,dataout,T,v,rho,u,s,p] = deal([]);
    %
    % Arrange user file
    if (updateuser)
        system(['cp ',champspath,'/user/userCase',num2str(j),'.F90 ',champspath,'/user/user.F90']);
    end
    %
    % Set up amr_runtime_parameters
    if (updateamr)
        system('cp amr_runtime_parameters_main amr_runtime_parameters');
        fid = fopen('amr_runtime_parameters_main','r');
        i = 1;
        tline = fgetl(fid);
        A{i} = tline;
        while ischar(tline)
            i = i+1;
            tline = fgetl(fid);
            A{i} = tline;
        end
        fclose(fid);
        A{4} = sprintf('%d                         ! nxb',nperblock);
        A{5} = sprintf('%d                         ! nyb',ny(j));
        A{6} = sprintf('%d                         ! nzb',1);
        fid = fopen('amr_runtime_parameters','w');
        for i = 1:numel(A)
            if A{i+1} == -1
                fprintf(fid,'%s', A{i});
                break
            else
                fprintf(fid,'%s\n', A{i});
            end
        end
    end
    %
    % Compute sampling points
    if (updatepoints)
        for i = 1:nprobes(j)
            x(i) = (i-1)*(xmax(j)-xmin(j))/(nprobes(j)-1);
            y(i) = yloc;
            z(i) = zloc;
        end
        %
        % Write out sampling points
        fileID = fopen([champspath,'/user/pointprobesFile.dat'],'w');
        fprintf(fileID,'%f %f %f \n',[x;y;z]);
        fclose(fileID);
    end
    %
    % Compile CHAMPS
    if (dorun)
        disp(['Compiling...']);
        system(['cd ',champspath,'; make main > scriptcompile.log']);
        %
        % Run CHAMPS
        disp(['Running...']);
        system(['cd ',champspath,'; mpirun -np ',num2str(np(j)),' BITCART input.dat -Dcase=',num2str(j-1),' > scriptrun.log']);
    end
    %
    % Read data
    if (updatedata)
        disp(['Processing/writing data...']);
        fileID = fopen([champspath,'/pointProbes/pointprobeOutput.dat'],'r');
        dum = fgetl(fileID);
        if (doreadlsf), dum = fgetl(fileID); end
        %
        % Read primitive variables
        datatext = fgetl(fileID);
        datatext = datatext(18:end);
        datain = cell2mat(textscan(datatext,'%f'));
        datain = reshape(datain(2:end),nvar,[])';
        %
        % Read level set
        if (doreadlsf)
            lsftext = fgetl(fileID);
            lsftext = lsftext(18:end);
            lsfin = cell2mat(textscan(lsftext,'%f'));
            lsf = reshape(lsfin(2:end),1,[])';
            gastype(lsf>0) = 1;
            gastype(lsf<=0) = 2;
        else
            lsf(1:nprobes-1) = 1; lsf = lsf';
            gastype(1:nprobes-1) = 1;
        end
        fclose(fileID);
        %
        % Compute properties
        p = datain(:,1);
        u = datain(:,2);
        v = datain(:,3);
        T = datain(:,4);
        for i = 1:nprobes(j)-1
            Ri = R(gastype(i),j);
            gammai = gamma(gastype(i),j);
            cpi = cp(gastype(i),j);
            cvi = cv(gastype(i),j);
            if (~isTait(gastype(i),j))
                rho(i) = p(i)/(Ri*T(i));
                % s(i) = log(p(i)/pinf) - gammai*log(p(i)*Tinf/(Ri*T(i)*pinf));
                s(i) = log(p(i)/pinf) - gammai*log(p(i)*Tinf/(Ri*T(i)*pinf));
            else
                rho(i) = rho0Tait*((p(i)-ATait)/BTait+1.0)^(1.0/gammai);
                s(i) = 0;
            end
            if (any(isTait(:,j))), rho(i) = log10(rho(i)); end
        end
        rho = rho'; s = 1e5*s';
        dataout=[lsf,rho,u,s,p];
        %
        % Write to final output file
        T = array2table([x(1:nprobes(j)-1)',dataout]);
        T.Properties.VariableNames(1:nvar+2) = {'x','lsf','rho','u','s','p'};
        writetable(T,[champspath,'/examples/case',num2str(j),'.dat'],'delimiter',' ');
    end
end
%
% Update slides
if (updateslides)
    disp(' ');
    disp(['Running pdfLaTeX to update slides...']);
    disp(' ');
    cd slides
    system(['pdflatex -halt-on-error main > /dev/null']);
    % system(['biber main > /dev/null']);
    % system(['pdflatex -halt-on-error main > /dev/null']);
    cd ../
end
%
% Ending message
disp(' ');
disp(['Done. Ran ',num2str(length(casestorun)),' case(s), wrote data successfully.']);
disp(' ');

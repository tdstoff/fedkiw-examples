#--------------------------------------------------------------------------
# BITCART Makefile for Ubuntu 14.04 LTS (GNU)
#--------------------------------------------------------------------------
export cur_dir := $(shell pwd)

# Set the location of the paramesh top directory
export paramesh_dir = $(BITCART_SOURCE_PATH)/paramesh
export user_dir := $(cur_dir)/user

# Define the fortran and C/C++ compilers
export FC  = mpif90
export CC  = mpicc
export CXX = mpic++

#------------------------------------------------------------------------------------------------------------------------------------
# Options from case.mk
#------------------------------------------------------------------------------------------------------------------------------------
include case.mk

#Optimization flag
ifndef OPTLEVEL
OPTLEVEL := 0
# OPTLEVEL := 3
endif

#Problem dimension
ifndef DIM
DIM := 2
endif

#Special case flag (WVN)
ifndef SPECIAL_CASE
SPECIAL_CASE := 0
endif

#CUDA (GPU) Flag
ifndef CUDA_ENABLE
CUDA_ENABLE := 0
endif

#Use HyWall Library
ifndef USEHYWALL
export USEHYWALL=0
endif

ifeq (${DIM}, 2)
export 3D_DIST=0
else
3D_DIST=1
endif

#Export options from case.mk file
export WM_OPT_LEVEL=${OPTLEVEL}
export PROBLEM_DIMENSION=${DIM}

#------------------------------------------------------------------------------------------------------------------------------------
# Compiler Flags (IFORT)
#------------------------------------------------------------------------------------------------------------------------------------
ifeq (${OPTLEVEL}, 0)
export FFLAGS   = -O0  -fbacktrace  -fbounds-check -g -cpp -finit-real=nan -Wall -ffree-line-length-none -fdefault-real-8 -fdefault-double-8 -fcray-pointer -w -I$(paramesh_dir)/headers
else
export FFLAGS   = -O3 -g -cpp -finit-real=nan -Wall -ffree-line-length-none -fdefault-real-8 -fdefault-double-8 -fcray-pointer -w -I$(paramesh_dir)/headers 
endif

export FFLAGS_par  = -O3 -g -cpp -finit-real=nan -Wall -ffree-line-length-none -fdefault-real-8 -fdefault-double-8 -fcray-pointer -w 

#---------------------------------------------------------------------------------------------------------------------------------------------------------------
#Pre-Compiler Flags for BITCART
#---------------------------------------------------------------------------------------------------------------------------------------------------------------
COMPILER_OPTS += -DPROBLEM_DIMENSION=${PROBLEM_DIMENSION}
COMPILER_OPTS += -DCUDA_ENABLE=${CUDA_ENABLE}
COMPILER_OPTS += -DBASEFLOW_MPI=0
COMPILER_OPTS += -DIBM_IBL=1
COMPILER_OPTS += -DUSEHYWALL=${USEHYWALL}
COMPILER_OPTS += -DSPECIAL_CASE=${SPECIAL_CASE}
COMPILER_OPTS += -Diins3d_dist=${3D_DIST}

#---------------------------------------------------------------------------------------------------------------------------------------------------------------
#Include Paths and Libraries
#---------------------------------------------------------------------------------------------------------------------------------------------------------------

#Include Paths
#---------------------------------------------------------------------------------------------------------------------------------------------------------------
#Paramesh
export IFLAGS   += -I$(paramesh_dir)/headers
#Zlib
#IFLAGS   += -I$(SOFTWARE)/zlib/zlib-install/include
#Navier Stokes
IFLAGS   += -I$(BITCART_SOURCE_PATH)/navier_stokes
#Karma
IFLAGS   += -I$(KARMA_UTIL_PATH)/include
#Wall Model (old)
IFLAGS   += -I$(BITCART_SOURCE_PATH)/wallFunction/lineletODE6/include -I$(BITCART_SOURCE_PATH)/../dependencies/eigen
#Parmetis
IFLAGS   += -I$(BITCART_SOURCE_PATH)/../dependencies/parmetis-4.0.3/install/include
IFLAGS   += -DIFORT=1 -DHDF5VERSION=1
#HDF5
IFLAGS   += -I$(BITCART_SOURCE_PATH)/../dependencies/hdf5-1.8.14/install/include
#Libraries
#---------------------------------------------------------------------------------------------------------------------------------------------------------------
#MPI
export ADD_LIB  = -L$(BITCART_SOURCE_PATH)/../dependencies/openmpi-1.8.4/install/lib -lmpi -lmpi_mpifh
#HDF5
export ADD_LIB += -L$(BITCART_SOURCE_PATH)/../dependencies/hdf5-1.8.14/install/lib  -lhdf5_fortran -lhdf5 -lhdf5_hl -lhdf5hl_fortran
#Zlib
#export ADD_LIB += -L$(SOFTWARE)/zlib/zlib-install/lib
#Lapack
export ADD_LIB += -L$(BITCART_SOURCE_PATH)/../dependencies/lapack-3.5.0 -llapack -lrefblas
#Paramesh
export ADD_LIB += -L$(paramesh_dir)/lib -L$(user_dir) -luser -lstdc++ -lm -ldl -I$(paramesh_dir)/headers  -lz -L/usr/local/gfortran/lib -lgfortran
#PETSc and Karma
export ADD_LIB += -L$(PETSC_DIR)/$(PETSC_ARCH)/lib -lpetsc -I$(PETSC_DIR)/$(PETSC_ARCH)/include -L$(KARMA_UTIL_PATH)/lib -lkarma_utils.OPT.MPI
#Parmetis and Metis
export ADD_LIB += -L$(BITCART_SOURCE_PATH)/../dependencies/parmetis-4.0.3/install/lib -lparmetis
export ADD_LIB += -L$(BITCART_SOURCE_PATH)/../dependencies/parmetis-4.0.3/metis/install/lib -lmetis

#Add Flags to Compiler Options
#------------------------------------------------------------------------------------------------------------------------------------
FFLAGS     += $(IFLAGS)
FFLAGS     += ${COMPILER_OPTS}
FFLAGS_par += $(IFLAGS)
export CFLAGS   = -cpp -Wuninitialized -g -std=c++11 ${COMPILER_OPTS}
export CXXFLAGS = -stdlib=libstdc++ -g ${COMPILER_OPTS}

#-----------------------------------------------
# SHMEM or MPI ?
# uncomment to use SHMEM
#export SHMEM = 1
#--------------------------------------------------------------------------

#########################################################################################################################################################
#Do Not Touch Unless You Know What You're Doing
#########################################################################################################################################################

.PHONY: all
ifdef SHMEM
all: libs headers source
else
all: libs headers mpi_source source user driver2
#data_structure inputs mathtools utilities surfacetool  motion turbulence cart_mesh  output  ibm post_processing navier_stokes driver2
endif

.PHONY: headers
headers:
	$(MAKE) -C $(paramesh_dir)/$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/headers/libmodules.a $(paramesh_dir)/libs

.PHONY: mpi_source
mpi_source: headers
	$(MAKE) -C $(paramesh_dir)/$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/mpi_source/libmpi_paramesh.a $(paramesh_dir)/libs

.PHONY: source
source: headers
	$(MAKE) -C $(paramesh_dir)/$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/source/libparamesh.a $(paramesh_dir)/libs

.PHONY: cart_mesh
cart_mesh: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../cart_mesh/libcartmesh.a $(paramesh_dir)/libs

.PHONY: navier_stokes
navier_stokes: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../navier_stokes/libnavierstokes.a $(paramesh_dir)/libs

.PHONY: ibm
ibm: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../ibm/libibm.a $(paramesh_dir)/libs


.PHONY: data_structure
data_structure: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../data_structure/libcartdata.a $(paramesh_dir)/libs

.PHONY: utilities
utilities: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../utilities/libcartutilities.a $(paramesh_dir)/libs

.PHONY: mathtools
mathtools: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../mathtools/libmathtools.a $(paramesh_dir)/libs

.PHONY: inputs
inputs: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../inputs/libinputreader.a $(paramesh_dir)/libs

.PHONY: material
material: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../material/libmaterialdata.a $(paramesh_dir)/libs

.PHONY: meshdomain
meshdomain: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../meshdomain/libmeshdomain.a $(paramesh_dir)/libs

.PHONY: output
output: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../output/liboutput.a $(paramesh_dir)/libs

.PHONY: post_processing
post_processing: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../post_processing/libpostprocessing.a $(paramesh_dir)/libs

.PHONY: turbulence
turbulence: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../turbulence/libturbulence.a $(paramesh_dir)/libs

.PHONY: motion
motion: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../motion/libmotion.a $(paramesh_dir)/libs

.PHONY: surfacetool
surfacetool: headers
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	mkdir -p $(paramesh_dir)/libs
	cp -f $(paramesh_dir)/../surfacetool/libsurfacetool.a $(paramesh_dir)/libs

.PHONY: user
user: headers
	$(MAKE) -C $(cur_dir)/$@ -f makefile.user

.PHONY: main
main:  
	$(RM) *~ $(paramesh_dir)/../driver2/main.o
	$(RM) *~ $(user_dir)/user.o
	$(MAKE) -C $(cur_dir)/user -f makefile.user
	$(MAKE) -C $(paramesh_dir)/../driver2 -f Makefile.gnu
	cp -f $(paramesh_dir)/../driver2/bin/BITCART $(cur_dir)    

.PHONY: realclean
realclean:
	$(RM) -r *~ $(paramesh_dir)/libs/* 
	for dir in headers mpi_source source;  do \
	  $(MAKE) -C $(paramesh_dir)/$$dir -f Makefile.gnu clean; \
	done
	$(MAKE) -C $(paramesh_dir)/../driver2 -f Makefile.gnu clean; \
	$(MAKE) -C $(user_dir) -f makefile.user clean; \

.PHONY: clean
clean:
	for dir in driver2;  do \
	  $(MAKE) -C $(paramesh_dir)/../$$dir -f Makefile.gnu clean; \
	done
	$(MAKE) -C $(user_dir) -f makefile.user clean; \
#	for dir in data_structure utilities surfacetool cart_mesh motion mathtools material output turbulence ibm post_processing navier_stokes driver;  do \

.PHONY: Tests
Tests: all
	$(MAKE) -C $(paramesh_dir)/$@ -f Makefile.gnu

# An example target to match an application directory name other than Tests
# in which the users application files are located.
.PHONY: driver
driver: 
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	cp -f $(paramesh_dir)/../driver/BITCART $(cur_dir)

.PHONY: driver2
driver2: 
	$(MAKE) -C $(paramesh_dir)/../$@ -f Makefile.gnu
	cp -f $(paramesh_dir)/../$@/bin/BITCART $(cur_dir)
libs:
	mkdir -p $(paramesh_dir)/libs

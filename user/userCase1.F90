#define x0Int     2.d0
#define RL        287.15d0
#define RR        287.15d0
#define rhoL      2.d0
#define rhoR      1.d0
#define uL        0.d0
#define uR        0.d0 
#define pL        9.8e5
#define pR        2.45e5
#define pi        3.1415926535897932384626433d0
!
#include "setup.h"
!
subroutine userSpecifiedFunction(xi,yi,zi,time,gamma,Rgas,qTemp)
  implicit none
  Real*8, intent(in)    :: xi,yi,zi,time,gamma,Rgas
  Real*8, intent(inout) :: qTemp(5)
  ! local
  Integer               :: s
  Real*8                :: gammaVec(2),heatCapVec(2),RgasVec(2),rho
  Real*8                :: p_prime,u_prime,v_prime,t_prime,rho_prime,w_prime
  !
  if (xi.lt.x0Int) then
     p_prime=pL
     T_prime=pL/(rhoL*RL)
     u_prime=uL
     v_prime=0.d0
     w_prime=0.d0
  else
     p_prime=pR
     T_prime=pR/(rhoR*RR)
     u_prime=uR
     v_prime=0.d0
     w_prime=0.d0
  endif
  !
  qTemp(1)  = p_prime
  qTemp(2)  = u_prime
  qTemp(3)  = v_prime
  qTemp(4)  = w_prime
  qTemp(5)  = t_prime
  !
end subroutine userSpecifiedFunction
!
subroutine userSpecifiedIC(xi,yi,zi,time,gamma,Rgas,qTemp)
  !
  implicit none
  Real*8, intent(in) :: xi,yi,zi,time,gamma,Rgas
  Real*8, intent(inout) :: qTemp(5)
  Real*8             :: ai,rhoi,presi,Mi,ri,r
  !
  call userSpecifiedFunction(xi,yi,zi,time,gamma,Rgas,qTemp)
  !
end subroutine userSpecifiedIC
!
!
!
!
!
!
!
!
!
!
subroutine userSpecifiedPhi(xi,yi,zi,time,data,numVars)
  !
  implicit none
  Real*8, intent(in)    :: xi,yi,zi,time
  Integer,intent(in)    :: numVars
  Real*8, intent(inout) :: data(numVars)  !rho,u,v,w,T
  ! local
  Real*8                :: rad2
  !
  data(:) = 0.d0
  !
end subroutine userSpecifiedPhi
!
subroutine userSpecifiedBaseflowIC(xi,yi,zi,time,gamma,Rgas,qTemp)
  !
  implicit none
  Real*8, intent(in) :: xi,yi,zi,time,gamma,Rgas
  Real*8, intent(inout) :: qTemp(5)
  Real*8                :: pref,Tref,vref
  !
  pref = 101327.
  Tref = 300.
  vref = 100.
  ! p
  qTemp(1)=pref
  ! u
  qTemp(2)=0.5*(2.d0+tanh(yi))*vref
  ! v
  qTemp(3)=0.d0
  ! w
  qTemp(4)=0.d0
  ! T
  qTemp(5)=Tref
  !
end subroutine userSpecifiedBaseflowIC
!
!                                xi,yi,zi,time,i,j,k,time,gamma,Rgas,Ma_inf,mu,nvar,unk1,idest,nxb,nyb,nzb,nguard,qTemp
subroutine userSpecifiedInflowBC(xi,yi,zi,i,j,k,time,gamma,Rgas,Ma_inf,mu,nvar,unk1,idest,nxb,nyb,nzb,nguard,qTemp)
  !
  implicit none
  real*8,intent(in)      :: xi,yi,zi,time,gamma,Rgas,Ma_inf,mu
  integer,intent(in)     :: nxb,nyb,nzb,nguard,i,j,k,idest,nvar
  real*8,intent(in)      :: unk1(1:4+iins3d,1:nxb+2*nguard,1:nyb+2*nguard,1:nzb+2*nguard*iins3d,1:2)
  real*8,intent(inout)   :: qTemp(5)
  !
  call userSpecifiedFunction(xi,yi,zi,time,gamma,Rgas,qTemp)
  !
end subroutine userSpecifiedInflowBC
!
subroutine userSpecifiedImmersedBC(xi,yi,zi,i,j,k,time,gamma,Rgas,Ma_inf,qTemp)
  !
  implicit none
  real*8,intent(in)      :: xi,yi,zi,time,gamma,Rgas,Ma_inf
  integer,intent(in)     :: i,j,k
  real*8,intent(inout)   :: qTemp(5)
  !
  call userSpecifiedFunction(xi,yi,zi,time,gamma,Rgas,qTemp)
  !
end subroutine userSpecifiedImmersedBC
!
!                                xi,yi,zi,i,j,k,time,gamma,Rgas,Ma_inf,mu,nvar,unk1,idest,nxb,nyb,nzb,nguard,qTemp
subroutine userSpecifiedOutflowBC(xi,yi,zi,i,j,k,time,gamma,Rgas,Ma_inf,mu,nvar,unk1,idest,nxb,nyb,nzb,nguard,qTemp)
  !
  implicit none
  real*8,intent(in)      :: xi,yi,zi,time,gamma,Rgas,Ma_inf,mu
  integer,intent(in)     :: nxb,nyb,nzb,nguard,i,j,k,idest,nvar
  real*8,intent(in)      :: unk1(1:4+iins3d,1:nxb+2*nguard,1:nyb+2*nguard,1:nzb+2*nguard*iins3d,1:2)
  real*8,intent(inout)   :: qTemp(5)
  !
  call userSpecifiedFunction(xi,yi,zi,time,gamma,Rgas,qTemp)
  !
end subroutine userSpecifiedOutflowBC
!
!                                xi,yi,zi,i,j,k,time,gamma,Rgas,Ma_inf,mu,nvar,unk1,idest,nxb,nyb,nzb,nguard,qTemp
subroutine userSpecifiedBottomBC(xi,yi,zi,i,j,k,time,gamma,Rgas,Ma_inf,mu,nvar,unk1,idest,nxb,nyb,nzb,nguard,qTemp)
  !
  implicit none
  real*8,intent(in)      :: xi,yi,zi,time,gamma,Rgas,Ma_inf,mu
  integer,intent(in)     :: nxb,nyb,nzb,nguard,i,j,k,idest,nvar
  real*8,intent(in)      :: unk1(1:4+iins3d,1:nxb+2*nguard,1:nyb+2*nguard,1:nzb+2*nguard*iins3d,1:2)
  real*8,intent(inout)   :: qTemp(5)
  !
  ! local
  real*8                 :: Vn,nx,ny,nz
  real*8                 :: Qvar(1:4+iins2d+iins3d)
  !
  call userSpecifiedFunction(xi,yi,zi,time,gamma,Rgas,qTemp)
  !
end subroutine userSpecifiedBottomBC
!
!                             xi,yi,zi,i,j,k,time,gamma,Rgas,Ma_inf,mu,nvar,unk1,idest,nxb,nyb,nzb,nguard,qTemp
subroutine userSpecifiedTopBC(xi,yi,zi,i,j,k,time,gamma,Rgas,Ma_inf,mu,nvar,unk1,idest,nxb,nyb,nzb,nguard,qTemp)
  !
  implicit none
  real*8,intent(in)      :: xi,yi,zi,time,gamma,Rgas,Ma_inf,mu
  integer,intent(in)     :: nxb,nyb,nzb,nguard,i,j,k,idest,nvar
  real*8,intent(in)      :: unk1(1:4+iins3d,1:nxb+2*nguard,1:nyb+2*nguard,1:nzb+2*nguard*iins3d,1:2)
  real*8,intent(inout)   :: qTemp(5)
  !
  ! local
  real*8                 :: ang,shock_speed,xini,xs
  !
  call userSpecifiedFunction(xi,yi,zi,time,gamma,Rgas,qTemp)
  !
end subroutine userSpecifiedTopBC
!
!                                xi,yi,zi,i,j,k,time,gamma,Rgas,Ma_inf,mu,nvar,unk1,idest,nxb,nyb,nzb,nguard,qTemp
subroutine userSpecifiedFrontBC(xi,yi,zi,i,j,k,time,gamma,Rgas,Ma_inf,mu,nvar,unk1,idest,nxb,nyb,nzb,nguard,qTemp)
  !
  implicit none
  real*8,intent(in)      :: xi,yi,zi,time,gamma,Rgas,Ma_inf,mu
  integer,intent(in)     :: nxb,nyb,nzb,nguard,i,j,k,idest,nvar
  real*8,intent(in)      :: unk1(1:4+iins3d,1:nxb+2*nguard,1:nyb+2*nguard,1:nzb+2*nguard*iins3d,1:2)
  real*8,intent(inout)   :: qTemp(5)
  !
  call userSpecifiedFunction(xi,yi,zi,time,gamma,Rgas,qTemp)
  !
end subroutine userSpecifiedFrontBC
!
!                                xi,yi,zi,i,j,k,time,gamma,Rgas,Ma_inf,mu,nvar,unk1,idest,nxb,nyb,nzb,nguard,qTemp
subroutine userSpecifiedBackBC(xi,yi,zi,i,j,k,time,gamma,Rgas,Ma_inf,mu,nvar,unk1,idest,nxb,nyb,nzb,nguard,qTemp)
  !
  implicit none
  real*8,intent(in)      :: xi,yi,zi,time,gamma,Rgas,Ma_inf,mu
  integer,intent(in)     :: nxb,nyb,nzb,nguard,i,j,k,idest,nvar
  real*8,intent(in)      :: unk1(1:4+iins3d,1:nxb+2*nguard,1:nyb+2*nguard,1:nzb+2*nguard*iins3d,1:2)
  real*8,intent(inout)   :: qTemp(5)
  !
  call userSpecifiedFunction(xi,yi,zi,time,gamma,Rgas,qTemp)
  !
end subroutine userSpecifiedBackBC
!
subroutine userSpecifiedgradPgradT(xi,yi,zi,time,distBC,Rgas,gamma,gradT,gradP)
  !
  implicit none
  real*8,intent(in)      :: xi,yi,zi,time,distBC,Rgas,gamma
  real*8                 :: gradT,gradP
  !
  ! local
  real*8                 :: radius,rhowall,utan2,exactdTdnVortex
  !
  gradT = 0.d0
  gradP = 0.d0
  !
end subroutine userSpecifiedgradPgradT
!
subroutine userSpecifiedVelocityGrad(xi,yi,zi,time,velocityGradient)
  !
  implicit none
  real*8,intent(in)      :: xi,yi,zi,time
  real*8,intent(out)     :: velocityGradient(3,3)
  !
#if (iins3d==0)
  ! u = sin(2.d0*pi*xi)*sin(2.d0*pi*yi)
  ! ux:
  velocityGradient(1,1)=2.d0*pi*cos(2.d0*pi*xi)*sin(2.d0*pi*yi)
  ! uy:
  velocityGradient(1,2)=2.d0*pi*sin(2.d0*pi*xi)*cos(2.d0*pi*yi)
  ! uz:
  velocityGradient(1,3)=0.d0
  !
  ! v = cos(2.d0*pi*xi)*cos(2.d0*pi*yi)
  ! vx:
  velocityGradient(2,1)=-2.d0*pi*sin(2.d0*pi*xi)*cos(2.d0*pi*yi)
  ! vy:
  velocityGradient(2,2)=-2.d0*pi*cos(2.d0*pi*xi)*sin(2.d0*pi*yi)
  ! vz:
  velocityGradient(2,3)=0.d0
  !
  ! w = 0
  ! wx:
  velocityGradient(3,1)=0.d0
  ! wy:
  velocityGradient(3,2)=0.d0
  ! wz:
  velocityGradient(3,3)=0.d0
  !
#else
  ! u = cos(2.d0*pi*xi)*sin(2.d0*pi*yi)*sin(2.d0*pi*zi)
  ! ux:
  velocityGradient(1,1)=-2.d0*pi*sin(2.d0*pi*xi)*sin(2.d0*pi*yi)*sin(2.d0*pi*zi)
  ! uy:
  velocityGradient(1,2)= 2.d0*pi*cos(2.d0*pi*xi)*cos(2.d0*pi*yi)*sin(2.d0*pi*zi)
  ! uz:
  velocityGradient(1,3)= 2.d0*pi*cos(2.d0*pi*xi)*sin(2.d0*pi*yi)*cos(2.d0*pi*zi)
  !
  ! v = sin(2.d0*pi*xi)*cos(2.d0*pi*yi)*sin(2.d0*pi*zi)
  ! vx:
  velocityGradient(2,1)= 2.d0*pi*cos(2.d0*pi*xi)*cos(2.d0*pi*yi)*sin(2.d0*pi*zi)
  ! vy:
  velocityGradient(2,2)=-2.d0*pi*sin(2.d0*pi*xi)*sin(2.d0*pi*yi)*sin(2.d0*pi*zi)
  ! vz:
  velocityGradient(2,3)= 2.d0*pi*sin(2.d0*pi*xi)*cos(2.d0*pi*yi)*cos(2.d0*pi*zi)
  !
  ! w = -2.d0*sin(2.d0*pi*xi)*sin(2.d0*pi*yi)*cos(2.d0*pi*zi)
  ! wx:
  velocityGradient(3,1)=-4.d0*pi*cos(2.d0*pi*xi)*sin(2.d0*pi*yi)*cos(2.d0*pi*zi)
  ! wy:
  velocityGradient(3,2)=-4.d0*pi*sin(2.d0*pi*xi)*cos(2.d0*pi*yi)*cos(2.d0*pi*zi)
  ! wz:
  velocityGradient(3,3)= 4.d0*pi*sin(2.d0*pi*xi)*sin(2.d0*pi*yi)*sin(2.d0*pi*zi)
  !
#endif
  !
end subroutine userSpecifiedVelocityGrad
!
subroutine userSpecifiedTemperatureGrad(xi,yi,zi,time,Rgas,gamma,temperatureGradient)
  !
  implicit none
  ! input
  real*8,intent(in)      :: xi,yi,zi,time,Rgas,gamma
  real*8,intent(out)     :: temperatureGradient(3)
  ! local
  real*8                 :: p  ,dpdx  ,dpdy  ,dpdz
  real*8                 :: rho,drhodx,drhody,drhodz
  real*8                 :: dTdx,dTdy,dTdz
  !
#if (iins3d==1)
  p   =0.5+0.2*cos(2.d0*pi*xi)*cos(2.d0*pi*yi)*cos(2.d0*pi*zi)
  dpdx=-0.4*pi*sin(2.d0*pi*xi)*cos(2.d0*pi*yi)*cos(2.d0*pi*zi)
  dpdy=-0.4*pi*cos(2.d0*pi*xi)*sin(2.d0*pi*yi)*cos(2.d0*pi*zi)
  dpdz=-0.4*pi*cos(2.d0*pi*xi)*cos(2.d0*pi*yi)*sin(2.d0*pi*zi)
#else
  p=0.5+0.2*cos(2.d0*pi*xi)*cos(2.d0*pi*yi)
  dpdx=-0.4*pi*sin(2.d0*pi*xi)*cos(2.d0*pi*yi)
  dpdy=-0.4*pi*cos(2.d0*pi*xi)*sin(2.d0*pi*yi)
#endif
  !
#if (iins3d==1)
  rho=0.5+0.2*sin(2.d0*pi*xi)*sin(2.d0*pi*yi)*sin(2.d0*pi*zi)
  drhodx=0.4*pi*cos(2.d0*pi*xi)*sin(2.d0*pi*yi)*sin(2.d0*pi*zi)
  drhody=0.4*pi*sin(2.d0*pi*xi)*cos(2.d0*pi*yi)*sin(2.d0*pi*zi)
  drhodz=0.4*pi*sin(2.d0*pi*xi)*sin(2.d0*pi*yi)*cos(2.d0*pi*zi)
#else
  rho=0.5+0.2*sin(2.d0*pi*xi)*sin(2.d0*pi*yi)
  drhodx=0.4*pi*cos(2.d0*pi*xi)*sin(2.d0*pi*yi)
  drhody=0.4*pi*sin(2.d0*pi*xi)*cos(2.d0*pi*yi)
#endif
  !
  dTdx=dpdx*1.0/(rho*Rgas)-p/(Rgas*rho**2)*drhodx
  dTdy=dpdy*1.0/(rho*Rgas)-p/(Rgas*rho**2)*drhody
#if (iins3d==1)
  dTdz=dpdz*1.0/(rho*Rgas)-p/(Rgas*rho**2)*drhodz
#else
  dTdz=0.d0
#endif
  ! Tx:
  temperatureGradient(1)=0.d0!dTdx
  ! Ty:
  temperatureGradient(2)=0.d0!dTdy
  ! Tz:
  temperatureGradient(3)=0.d0!dTdz
  !
end subroutine userSpecifiedTemperatureGrad
!
subroutine userSpecifiedStretching(xComp,xPhys)
  !
  implicit none
  !
  ! input
  real*8,intent(in)      :: xComp(2+iins3d)
  real*8,intent(out)     :: xPhys(2+iins3d)
  !
  ! computational mesh
  ! xComp(1) = xi
  ! xComp(2) = eta
  ! xComp(3) = zeta
  !
  ! physical mesh
  ! xPhys(1) = x
  ! xPhys(2) = y
  ! xPhys(3) = z
  !
  ! specify stretching functions
  !
  ! x(xi,eta,zeta)
  xPhys(1)=xComp(1)
  !
  ! x(xi,eta,zeta)
  xPhys(2)=xComp(2)
  !
#if(iins3d==1)
  ! x(xi,eta,zeta)
  xPhys(3)=xComp(3)
#endif
  !
end subroutine userSpecifiedStretching
!
! User Specified derivatives of grid stretching functions
subroutine userSpecifiedStretchingDerivative(xi,eta,zeta,metricInv)
  !
  implicit none
  !
  ! input
  real*8,intent(in)      :: xi,eta,zeta
  real*8,intent(out)     :: metricInv(4+5*iins3d)
  !
  ! Inverse metric terms
  !
  ! metricInv(1) = d(x)/dxi
  ! metricInv(2) = d(x)/deta
  ! metricInv(3) = d(y)/dxi
  ! metricInv(4) = d(y)/deta
  ! metricInv(5) = d(x)/dzeta
  ! metricInv(6) = d(y)/dzeta
  ! metricInv(7) = d(z)/dxi
  ! metricInv(8) = d(z)/deta
  ! metricInv(9) = d(z)/dzeta
  !
  ! Provide derivatives of stretching functions
  metricInv(1)=1.d0
  metricInv(2)=0.d0
  metricInv(3)=0.d0
  metricInv(4)=1.d0
  !
#if(iins3d==1)
  metricInv(5)=0.d0
  metricInv(6)=0.d0
  metricInv(7)=0.d0
  metricInv(8)=0.d0
  metricInv(9)=1.d0
#endif
  !
end subroutine userSpecifiedStretchingDerivative
!
Real*8 function visc_law(T,Tinf)
  !
  !
  IMPLICIT NONE
  !
  REAL*8, INTENT(IN)      ::   T,Tinf
  !
  ! local variables
  REAL*8                  ::   C,Tref
  parameter (C=110.4d0,Tref=273.15)
  !
  ! Re = ~100,000
  visc_law = 1.45151376745308d-06*T**1.5d0/(T+110.4d0)
  !
  return

  !
end function visc_law

REAL*8 function dmu_dT(T,Tinf)
  !
  IMPLICIT NONE
  !
  REAL*8, INTENT(IN)      ::   T,Tinf
  !
  ! local variables
  REAL*8                  ::   k1,k2
  !
  dmu_dT = 1.45151376745308d-06*(1.5d0*T**0.5d0/(T+110.4d0)-T**1.5/((T+110.4d0)*(T+110.4d0)))
  !
  return
end function dmu_dT
